import Vue from "vue";

export default {
    setDocuments: (state, payload) => {
        Vue.set(state, "list", payload);
    },
    setDocument: (state, payload) => {
        Vue.set(state, "document", payload);
    },
    removeDocument: (state, id) => {
        const index = state.list.findIndex(item => item._id === id);
        Vue.delete(state.list, index);
    },
};