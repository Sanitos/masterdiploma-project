import {Document} from "../resources";

export default {
    save: ({ commit }, payload) => new Promise((resolve, reject)=>{
        Document.save(payload).then(({body})=>{
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    update: ({ commit }, {payload, id}) => new Promise((resolve, reject)=>{
        Document.update({payload, id}).then(({body})=>{
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    send: ({ commit }, payload) => new Promise((resolve, reject)=>{
        Document.send(payload).then(({body})=>{
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    sign: ({ commit }, payload) => new Promise((resolve, reject)=>{
        Document.sign(payload).then(({body})=>{
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    list: ({ commit }) => new Promise((resolve, reject)=>{
        Document.list().then(({body})=>{
            commit("setDocuments", body);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    getDocument: ({ commit }, {id}) => new Promise((resolve, reject)=>{
        Document.getDocument({params: {id}}).then(({body})=>{
            commit("setDocument", body);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    remove: ({ commit }, {id}) => new Promise((resolve, reject)=>{
        Document.remove({params: {id}}).then(({body})=>{
            commit("removeDocument", id);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
};