import Vue from "vue";
import Vuex from "vuex";
import createLogger from "vuex/dist/logger";
import auth from "./auth/index"
import user from "./user/index"
import document from "./document/index"
import sentDocument from "./sentDocument/index"


Vue.use(Vuex);

const debug = true;

const store = new Vuex.Store({
  modules: {
    auth,
    user,
    document,
    sentDocument,
  },

  strict: debug,

  plugins: debug ? [createLogger()] : [],
});

export default store;