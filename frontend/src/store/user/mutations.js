import Vue from "vue";

export default {
    setCurrentUser: (state, payload) => {
        Vue.set(state, "currentUser", payload);
    },
    setUsers: (state, payload) => {
        Vue.set(state, "list", payload);
    },
};