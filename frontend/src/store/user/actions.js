import {User} from "../resources";

export default {
    getUserInfo: ({ commit }, payload) => new Promise((resolve, reject)=>{
        User.getUserInfo().then(({body})=>{
            commit("setCurrentUser", body);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    getById: ({ commit }, {id}) => new Promise((resolve, reject)=>{
        User.getById({params: {id}}).then(({body})=>{
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    getList: ({ commit }) => new Promise((resolve, reject)=>{
        User.list().then(({body})=>{
            commit("setUsers", body);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
};