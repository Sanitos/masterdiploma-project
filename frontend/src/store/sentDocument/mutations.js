import Vue from "vue";

export default {
    setDocuments: (state, payload) => {
        Vue.set(state, "listPending", payload);
    },
    setDocumentsSigned: (state, payload) => {
        Vue.set(state, "listSigned", payload);
    },
    setDocument: (state, payload) => {
        Vue.set(state, "document", payload);
    },
};