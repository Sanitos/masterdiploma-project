import {SentDocument} from "../resources";

export default {
    listPending: ({ commit, state }) => new Promise((resolve, reject)=>{
        SentDocument.listPending().then(({body})=>{
            commit("setDocuments", body);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
    listSigned: ({ commit, state }) => new Promise((resolve, reject)=>{
        SentDocument.listSigned().then(({body})=>{
            commit("setDocumentsSigned", body);
            resolve(body);
        }, ({body}) => {
            reject(body);
        });
    }),
};