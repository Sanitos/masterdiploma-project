import Vue from "vue";
const backendUrl = (url) => {
    return `http://localhost:3000/${url}`
}

export const Auth = {
    login: (payload) => Vue.http.post(backendUrl("auth/login"), payload),
    signup: (payload) => Vue.http.post(backendUrl("auth/register"), payload)
};
export const User = {
    getUserInfo: () => Vue.http.get(backendUrl("users/profile")),
    getById: (payload) => Vue.http.get(backendUrl("users/{id}"), payload),
    list: () => Vue.http.get(backendUrl("users"))
};
export const Document = {
    save: (payload) => Vue.http.post(backendUrl("documents"), payload),
    update: ({id, payload}) => Vue.http.put(backendUrl("documents/{id}"), payload, {params: {id}}),
    list: () => Vue.http.get(backendUrl("documents")),
    getDocument: (payload) => Vue.http.get(backendUrl("documents/{id}"), payload),
    send: (payload) => Vue.http.post(backendUrl("documents/send"), payload),
    remove: (payload) => Vue.http.delete(backendUrl("documents/{id}"), payload),
    sign: ({id, payload}) => Vue.http.put(backendUrl("documents/{id}/sign"), payload, {params: {id}}),
};

export const SentDocument = {
    listPending: () => Vue.http.get(backendUrl("sent_documents")),
    listSigned: () => Vue.http.get(backendUrl("sent_documents/signed")),
};