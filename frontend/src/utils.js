export const setAuthToken = (token) => localStorage.setItem('auth_token', token);
export const getAuthToken = () => localStorage.getItem('auth_token');
export const isAuthenticated = () => !!getAuthToken();