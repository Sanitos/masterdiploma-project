const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');

const documentSchema = new mongoose.Schema({
  content: {
    type: String,
  },
  type: {
    type: String,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  fileId: {
    type: String,
  },
  tags: {
    type: Array,
    default: [],
  },
  authorId: {
    type: String,
    required: true,
  },
}, {
  timestamps: true,
});
documentSchema.statics = {
  async getById(id) {
    try {
      let document;

      if (mongoose.Types.ObjectId.isValid(id)) {
        document = await this.findById(id).exec();
      }
      if (document) {
        return document;
      }

      throw new APIError({
        message: 'Document not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  list() {
    return this.find({})
      .sort({ createdAt: -1 })
      .exec();
  },
  async removeById(id) {
    try {
      let document;

      if (mongoose.Types.ObjectId.isValid(id)) {
        document = await this.remove({ _id: id }).exec();
      }
      if (document) {
        return document;
      }

      throw new APIError({
        message: 'Document not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  async updateById(id, obj) {
    try {
      let document;

      if (mongoose.Types.ObjectId.isValid(id)) {
        document = await this.updateOne({ _id: id }, obj).exec();
      }
      if (document) {
        return document;
      }

      throw new APIError({
        message: 'Document not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },

};

module.exports = mongoose.model('Document', documentSchema);
