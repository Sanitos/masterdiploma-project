const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');

const attachmentSchema = new mongoose.Schema({
  base64: {
    type: String,
    required: true,
  },
  mimeType: {
    type: String,
    required: true,
  },
}, {
  timestamps: true,
});
attachmentSchema.statics = {
  async getById(id) {
    try {
      let document;

      if (mongoose.Types.ObjectId.isValid(id)) {
        document = await this.findById(id).exec();
      }
      if (document) {
        return document;
      }

      throw new APIError({
        message: 'Document not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
};

module.exports = mongoose.model('Attachment', attachmentSchema);
