const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');

const documentSentSchema = new mongoose.Schema({
  recipient: {
    type: String,
    required: true,
  },
  sender: {
    type: String,
    required: true,
  },
  documentId: {
    type: String,
    required: true,
  },
  status: { type: String, default: 'pending' },
  signature: { type: String, default: null },
}, {
  timestamps: true,
});
documentSentSchema.statics = {
  async getById(id) {
    try {
      let document;

      if (mongoose.Types.ObjectId.isValid(id)) {
        document = await this.findById(id).exec();
      }
      if (document) {
        return document;
      }

      throw new APIError({
        message: 'Document not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  async removeById(id) {
    try {
      let document;

      if (mongoose.Types.ObjectId.isValid(id)) {
        document = await this.remove({ _id: id }).exec();
      }
      if (document) {
        return document;
      }

      throw new APIError({
        message: 'Document not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  async updateById(id, obj) {
    try {
      let document;

      if (mongoose.Types.ObjectId.isValid(id)) {
        document = await this.updateOne({ _id: id }, obj).exec();
      }
      if (document) {
        return document;
      }

      throw new APIError({
        message: 'Document not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  getByStatus(status) {
    return this.find({ status })
      .sort({ createdAt: -1 })
      .exec();
  },
};

module.exports = mongoose.model('SentDocument', documentSentSchema);
