const mongoose = require('mongoose');
const httpStatus = require('http-status');
const APIError = require('../utils/APIError');

const tagSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
  },
  description: {
    type: String,
  },
}, {
  timestamps: true,
});
tagSchema.statics = {
  async getById(id) {
    try {
      let tag;

      if (mongoose.Types.ObjectId.isValid(id)) {
        tag = await this.findById(id).exec();
      }
      if (tag) {
        return tag;
      }

      throw new APIError({
        message: 'Tag not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  list() {
    return this.find({})
      .sort({ createdAt: -1 })
      .exec();
  },
  async removeById(id) {
    try {
      let tag;

      if (mongoose.Types.ObjectId.isValid(id)) {
        tag = await this.remove({ _id: id }).exec();
      }
      if (tag) {
        return tag;
      }

      throw new APIError({
        message: 'Tag not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },
  async updateById(id, obj) {
    try {
      let tag;

      if (mongoose.Types.ObjectId.isValid(id)) {
        tag = await this.updateOne({ _id: id }, obj).exec();
      }
      if (tag) {
        return tag;
      }

      throw new APIError({
        message: 'Tag not found',
        status: httpStatus.NOT_FOUND,
      });
    } catch (error) {
      throw error;
    }
  },

};

module.exports = mongoose.model('Tag', tagSchema);
