const Joi = require('joi');

module.exports = {
  saveCreated: {
    body: {
      content: Joi.string()
        .required(),
    },
  },
};
