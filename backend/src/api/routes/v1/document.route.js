const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/document.controller');
const sentDocumentController = require('../../controllers/sent_document.controller');
const {
  saveCreated,
} = require('../../validations/document.validation');

const router = express.Router();

router
  .route('/')
  .post(controller.saveCreated);
router
  .route('/')
  .get(controller.list);
router
  .route('/:documentId')
  .get(controller.getDocument)
  .delete(controller.delete)
  .put(controller.update);

router
  .route('/send')
  .post(sentDocumentController.create);
router
  .route('/:documentId/sign')
  .put(sentDocumentController.sign);


module.exports = router;
