const express = require('express');
const userRoutes = require('./user.route');
const authRoutes = require('./auth.route');
const documentRoutes = require('./document.route');
const attachmentRoutes = require('./attachment.route');
const sentDocumentsRoutes = require('./sent_document.route');
const tagRoutes = require('./tag.route');

const router = express.Router();


router.get('/status', (req, res) => res.send('OK'));

router.use('/users', userRoutes);
router.use('/auth', authRoutes);
router.use('/documents', documentRoutes);
router.use('/sent_documents', sentDocumentsRoutes);
router.use('/attachments', attachmentRoutes);
router.use('/tags', tagRoutes);

module.exports = router;
