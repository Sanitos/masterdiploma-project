const express = require('express');
const controller = require('../../controllers/sent_document.controller');

const router = express.Router();

router
  .route('/')
  .get(controller.pendingList);
router
  .route('/signed')
  .get(controller.signedList);

module.exports = router;
