const express = require('express');
const controller = require('../../controllers/tag.controller');

const router = express.Router();

router
  .route('/')
  .post(controller.create)
  .get(controller.list);
router
  .route('/:tagId')
  .get(controller.getTag)
  .delete(controller.delete)
  .put(controller.update);

module.exports = router;
