const express = require('express');
const controller = require('../../controllers/attachment.controller');
const multer = require('multer');

const storage = multer.diskStorage({
  destination(req, file, cb) {
    cb(null, 'uploads');
  },
  filename(req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}`);
  },
});

const upload = multer({ storage });
const router = express.Router();

router
  .route('/')
  .post(upload.single('file'), controller.create);
router
  .route('/:fileId')
  .get(controller.get);

module.exports = router;
