const fs = require('fs');
const httpStatus = require('http-status');
const { omit } = require('lodash');
const Attachment = require('../models/attachment.model');

exports.create = async (req, res) => {
  try {
    const img = fs.readFileSync(req.file.path);
    const encodedFile = img.toString('base64');
    const attachment = new Attachment({ base64: `${encodedFile}`, mimeType: req.file.mimetype});
    const savedAttachment = await attachment.save();
    res.json({ id: savedAttachment._id });
  } catch (error) {
    console.log(error);
  }
};
exports.get = async (req, res) => {
  try {
    const { fileId } = req.params;
    const file = await Attachment.getById(fileId);
    const img = Buffer.from(file.base64, 'base64');

    res.writeHead(200, {
      'Content-Type': file.mimeType,
      'Content-Length': img.length,
    });

    res.end(img);
  } catch (error) {
    console.log(error);
  }
};

