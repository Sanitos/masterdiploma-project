const httpStatus = require('http-status');
const { omit } = require('lodash');
const SentDocument = require('../models/sent_document.model');

exports.create = async (req, res) => {
  try {
    const doc = new SentDocument(req.body);
    const savedDoc = await doc.save();
    res.status(httpStatus.CREATED);
    res.json(savedDoc);
  } catch (error) {
    console.log(error);
  }
};
exports.delete = async (req, res) => {
  try {
    const { documentId } = req.params;
    const doc = await SentDocument.removeById(documentId);
    res.json(doc);
  } catch (error) {
    console.log(error);
  }
};
exports.getDocument = async (req, res) => {
  try {
    const { documentId } = req.params;
    const doc = await SentDocument.getById(documentId);
    res.json(doc);
  } catch (error) {
    console.log(error);
  }
};
exports.sign = async (req, res) => {
  try {
    const { documentId } = req.params;
    const obj = { signature: req.body.signature, status: 'signed' };
    console.log(obj);
    const doc = await SentDocument.updateById(documentId, obj);
    res.json(doc);
  } catch (error) {
    console.log(error);
  }
};
exports.pendingList = async (req, res) => {
  try {
    const docs = await SentDocument.getByStatus('pending');
    res.json(docs);
  } catch (error) {
    console.log(error);
  }
};
exports.signedList = async (req, res) => {
  try {
    const docs = await SentDocument.getByStatus('signed');
    res.json(docs);
  } catch (error) {
    console.log(error);
  }
};

