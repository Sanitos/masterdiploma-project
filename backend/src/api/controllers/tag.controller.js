const httpStatus = require('http-status');
const { omit } = require('lodash');
const Tag = require('../models/tag.model');

exports.create = async (req, res) => {
  try {
    const tag = new Tag(req.body);
    const savedTag = await tag.save();
    res.status(httpStatus.CREATED);
    res.json(savedTag);
  } catch (error) {
    console.log(error);
  }
};
exports.getTag = async (req, res) => {
  try {
    const { tagId } = req.params;
    const tag = await Tag.getById(tagId);
    res.json(tag);
  } catch (error) {
    console.log(error);
  }
};
exports.delete = async (req, res) => {
  try {
    const { tagId } = req.params;
    const tag = await Tag.removeById(tagId);
    res.json(tag);
  } catch (error) {
    console.log(error);
  }
};
exports.update = async (req, res) => {
  try {
    const { tagId } = req.params;
    const tag = await Tag.updateById(tagId, req.body);
    res.json(tag);
  } catch (error) {
    console.log(error);
  }
};
exports.list = async (req, res) => {
  try {
    const tags = await Tag.list();
    res.json(tags);
  } catch (error) {
    console.log(error);
  }
};

