const httpStatus = require('http-status');
const { omit } = require('lodash');
const Document = require('../models/document.model');
const Attachment = require('../models/attachment.model');

exports.saveCreated = async (req, res) => {
  try {
    const doc = new Document(req.body);
    const savedDoc = await doc.save();
    res.status(httpStatus.CREATED);
    res.json(savedDoc);
  } catch (error) {
    console.log(error);
  }
};
exports.getDocument = async (req, res) => {
  try {
    const { documentId } = req.params;
    const doc = await Document.getById(documentId);
    let mimeType;
    if (doc.fileId) {
      const attachment = await Attachment.getById(doc.fileId);
      mimeType = attachment.mimeType;
    }
    res.json({...doc._doc, mimeType});
  } catch (error) {
    console.log(error);
  }
};
exports.delete = async (req, res) => {
  try {
    const { documentId } = req.params;
    const doc = await Document.removeById(documentId);
    res.json(doc);
  } catch (error) {
    console.log(error);
  }
};
exports.update = async (req, res) => {
  try {
    const { documentId } = req.params;
    const doc = await Document.updateById(documentId, req.body);
    res.json(doc);
  } catch (error) {
    console.log(error);
  }
};
exports.list = async (req, res) => {
  try {
    const docs = await Document.list();
    res.json(docs);
  } catch (error) {
    console.log(error);
  }
};

